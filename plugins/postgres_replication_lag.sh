#!/bin/bash
USER=gitlab-psql
WARNING=500

SLAVE=`ps -ef|grep -c "wal\ receiver"`
if [ "$SLAVE" -ne "0" ]; then
  echo "0 PostgreSQL_replication_lag pg_repl_lag=0;$WARNING;;0; OK - wal receiver detected: this check is for master only"
  exit
fi

LAG_BYTES="$(echo "select pg_xlog_location_diff(pg_current_xlog_insert_location(), flush_location) from pg_stat_replication;" |\
    su - $USER -c "psql -h /var/opt/gitlab/postgresql --variable ON_ERROR_STOP=1 -d postgres -A -t -F' '" 2>/dev/null)"

LAG_KBYTES=`expr $LAG_BYTES / 1024`

if [ "$LAG_KBYTES" = "" ]; then
  echo "2 PostgreSQL_replication_lag pg_repl_lag=0;500;;0; CRITICAL - replication stopped"
  exit
fi

if [ "$LAG_KBYTES" -gt "$WARNING" ]; then
  echo "1 PostgreSQL_replication_lag pg_repl_lag=$LAG_KBYTES;$WARNING;;0; WARNING - replication lag to high: $LAG_KBYTES KB"
  exit
fi

echo "0 PostgreSQL_replication_lag pg_repl_lag=$LAG_KBYTES;$WARNING;;0; OK - replication lag: $LAG_KBYTES KB"
