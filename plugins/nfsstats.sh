#!/bin/bash
retrans_file=0
if [ -f /tmp/checkmk_nfs_retrans ]; then
    retrans_file=$(cat /tmp/checkmk_nfs_retrans)
fi
retrans_cur=$(nfsstat -rc|head -3|tail -1|awk '{ print $2}')
echo $retrans_cur > /tmp/checkmk_nfs_retrans

retrans=$(expr $retrans_cur - $retrans_file)
if [ $retrans -lt 1 ]; then
    status=0
    statustxt=OK
elif [ $retrans -lt 5 ]; then
    status=1
    statustxt=WARNING
else
    status=2
    statustxt=CRITICAL
fi
echo "$status NFS_retrans retrans=$retrans;1;5;0; $statustxt - NFS retrans counts $retrans"
