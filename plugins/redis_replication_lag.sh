#!/bin/bash
WARNING=500

MASTER=`/opt/gitlab/embedded/bin/redis-cli info | grep -c role:master`
if [ "$MASTER" -eq "0" ]; then
  echo "0 Redis_replication_lag redis_repl_lag=0;$WARNING;;0; OK - role slave detected: this check is for master only"
  exit
fi

MASTER_OFFSET=`/opt/gitlab/embedded/bin/redis-cli info | grep master_repl_offset | awk -F: '{ print $2 }' | tr -d '\r'`
SLAVE_OFFSET=`/opt/gitlab/embedded/bin/redis-cli info | grep slave0 | awk -F, '{ print $4 }' | sed -e 's/offset=//' | tr -d '\r'`
LAG_BYTES=`expr $MASTER_OFFSET - $SLAVE_OFFSET`
LAG_KBYTES=`expr $LAG_BYTES / 1024`

if [ "$LAG_KBYTES" = "" ]; then
  echo "2 Redis_replication_lag redis_repl_lag=0;500;;0; CRITICAL - replication stopped"
  exit
fi

if [ "$LAG_KBYTES" -gt "$WARNING" ]; then
  echo "1 Redis_replication_lag redis_repl_lag=$LAG_KBYTES;$WARNING;;0; WARNING - replication lag to high: $LAG_KBYTES KB"
  exit
fi

echo "0 Redis_replication_lag redis_repl_lag=$LAG_KBYTES;$WARNING;;0; OK - replication lag: $LAG_KBYTES KB"
