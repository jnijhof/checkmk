#!/bin/bash
# Slack
SLACK_WEBHOOK="https://hooks.slack.com/services/..."
CHANNEL="#operations"
USERNAME="alerts"
ICON=":bangbang:"
MSG="Check_MK: $NOTIFY_HOSTNAME is $NOTIFY_HOSTSTATE"

curl -X POST --data-urlencode "payload={\"channel\": \"$CHANNEL\", \"username\": \"$USERNAME\", \"text\": \"<!channel> $MSG\", \"icon_emoji\": \"$ICON\"}" $SLACK_WEBHOOK >/dev/null 2>&1
